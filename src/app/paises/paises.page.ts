import { Component, OnInit } from '@angular/core';
import { Pais} from './pais';

@Component({
  selector: 'app-paises',
  templateUrl: './paises.page.html',
  styleUrls: ['./paises.page.scss'],
})
export class PaisesPage implements OnInit {

  Paises : Pais[]

  constructor() { 
    this.Paises = [
        new Pais("Argentina"),
        new Pais("Ecuador"),
        new Pais("Brazil"),
        new Pais("Colombia"),
        new Pais("Bolivia"),
        new Pais("Estados Unidos"),
        new Pais("Canada"),
    
    ]
  }
  
  ngOnInit() {}
}
